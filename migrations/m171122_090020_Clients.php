<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m171122_090020_Clients
 */
class m171122_090020_Clients extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('Clients', [
            'id' => $this->primaryKey()->comment('Идентификатор'),
            'client_snp' => $this->string(500)->defaultValue(null)->comment('ФИО клиента'),
            'client_phone' => $this->string(500)->defaultValue(null)->comment('Телефон клиента'),
            'updated_at' => $this->timestamp()->defaultValue(null)->comment('Дата и время создания'),
            'created_at' => $this->timestamp()->defaultValue(null)->comment('Дата и время редактирования'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('Clients');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171122_090020_Clients cannot be reverted.\n";

        return false;
    }
    */
}
