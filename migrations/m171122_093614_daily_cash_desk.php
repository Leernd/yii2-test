<?php

use yii\db\Migration;

/**
 * Class m171122_093614_daily_cash_desk
 */
class m171122_093614_daily_cash_desk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('daily_cash_desk', [
            'id' => $this->primaryKey()->comment('Идентификатор'),
            'payment_day' => $this->dateTime()->defaultValue(null)->comment('За какой день операции'),
            'administrator' => $this->string(255)->defaultValue(null)->comment('ИД пользователя администратора'),
            'begin_saldo' => $this->float(15,2)->defaultValue(null)->comment('сумма на начало дня'),
            'updated_at' => $this->timestamp()->defaultValue(null)->comment('Дата и время создания'),
            'created_at' => $this->timestamp()->defaultValue(null)->comment('Дата и время редактирования'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('daily_cash_desk');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171122_093614_daily_cash_desk cannot be reverted.\n";

        return false;
    }
    */
}
