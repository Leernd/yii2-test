<?php

use yii\db\Migration;

/**
 * Class m171122_094913_receipts_of_money
 */
class m171122_094913_receipts_of_money extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('receipts_of_money', [
            'id' => $this->primaryKey()->comment('Идентификатор'),
            'client_id' => $this->integer()->defaultValue(null)->comment('Ссылка на клиента школы'),
            'payment_sum' => $this->float(15,2)->defaultValue(null)->comment('Сумма оплаты от клиента'),
            'updated_at' => $this->timestamp()->defaultValue(null)->comment('Дата и время создания'),
            'created_at' => $this->timestamp()->defaultValue(null)->comment('Дата и время редактирования'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('receipts_of_money');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171122_094913_receipts_of_money cannot be reverted.\n";

        return false;
    }
    */
}
