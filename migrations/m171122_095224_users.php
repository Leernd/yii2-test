<?php

use yii\db\Migration;

/**
 * Class m171122_095224_users
 */
class m171122_095224_users extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey()->comment('Идентификатор'),
            'login' => $this->string(32)->defaultValue(null)->comment('Логин юзера'),
            'password' => $this->string(32)->defaultValue(null)->comment('Пароль юзера'),
            'updated_at' => $this->timestamp()->defaultValue(null)->comment('Дата и время создания'),
            'created_at' => $this->timestamp()->defaultValue(null)->comment('Дата и время редактирования'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171122_095224_users cannot be reverted.\n";

        return false;
    }
    */
}
