<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "daily_cash_desk".
 *
 * @property integer $id
 * @property string $payment_day
 * @property integer $administrator
 * @property double $begin_saldo
 * @property string $updated_at
 * @property string $created_at
 */
class Cash extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daily_cash_desk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['updated_at', 'created_at'], 'safe'],
            [['administrator'], 'string'],
            [['begin_saldo'], 'number'],
            [['payment_day'],'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_day' => 'Дата',
            'administrator' => "Администратор",
            'begin_saldo' => 'Begin Saldo',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

}
