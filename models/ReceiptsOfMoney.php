<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "receipts_of_money".
 *
 * @property integer $id
 * @property integer $client_id
 * @property double $payment_sum
 * @property string $updated_at
 * @property string $created_at
 */
class ReceiptsOfMoney extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'receipts_of_money';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id'], 'integer'],
            [['payment_sum'], 'number'],
            [['updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'payment_sum' => 'Payment Sum',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    public function getClients()
    {
        return $this->hasMany(client_snp::className(), ['id' => 'client_id']);
    }
}
