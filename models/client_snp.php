<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Clients".
 *
 * @property integer $id
 * @property string $client_snp
 * @property string $client_phone
 * @property string $updated_at
 * @property string $created_at
 */
class client_snp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['updated_at', 'created_at'], 'safe'],
            [['client_snp', 'client_phone'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_snp' => 'Client Snp',
            'client_phone' => 'Client Phone',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    public function getReceiptOfMany()
    {
        return $this->hasMany(ReceiptsOfMoney::className(), ['client_id' => 'id']);
    }
}
