<?Header('Content-Type: text/html; charset=utf-8'); ?>
<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\Cash */
/* @var $form ActiveForm */
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>
    <meta charset="utf-8" />
</head>
<style>
    table{
        text-align: center;
    }
    .font{
        font-weight: bold;
    }
    .blocks{
        margin: 15px;
    }
</style>

<body>

<p>
    <button class="btn btn-success" >Добавить дневную кассу</button>
</p>
<form method="POST" action="index.php?r=cash/query">
<div class="blocks">
    <label class="control-label" for="inputSuccess1">Дата</label>
    <input type="date" class="form-control" id="inputSuccess1" name="time">

    <label class="control-label" for="inputSuccess1">Администратор</label>
    <select class="form-control" style="width:200px" name="administrator">
        <option selected disabled> </option>
        <option value="Ivanov A.A">Ivanov A.A</option>
        <option value="Andreychuk A.A">Andreychuk A.A</option>
        <option value="Vadimchuk A.A">Vadimchuk A.A</option>
    </select>

    <label class="control-label" for="inputSuccess1">Сумма на начало</label>
    <input type="text" class="form-control" id="inputSuccess1" name="sum">
    <p>
        <input type="button" class="btn btn-warning" style="margin-top: 10px" id="addRow" value="Добавить строчку">
    </p>
    <table class="table" border="1" id="example">
        <thead>
        <tr>
            <td><b>Клиент</b></td>
            <td><b>Сумма oплаты</b></td>
        </tr>
        </thead>
        <tbody>
        <?php
        $arr = [];
        foreach ($data as $value)
            {
                $receiptOfMany = null;
                $payment_sum = null;
                $client_snp = null;
                    if(isset($value['receiptOfMany'][0]) &&  isset($value['client_snp']))
                    {
                        $receiptOfMany = $value['receiptOfMany'][0]['payment_sum'] ;
                        $payment_sum = $value['receiptOfMany'][0]['payment_sum'];
                        $client_snp = $value['client_snp'];

                        echo '<tr>';
                        echo '<td>'.$client_snp.'</td>';
                        echo '<td>'.$payment_sum.'</td>';
                        echo '</tr>';
                    }
                $arr[] =$receiptOfMany;
            }
            ?>
        </tbody>

    </table>
    <label class="control-label" for="inputSuccess1">Итого поступлений</label>
    <input class="form-control" id="disabledInput" type="text" disabled value="<?php echo count($data);?>">

    <label class="control-label" for="inputSuccess1">Сумма на конец</label>
    <input class="form-control" id="disabledInput" type="text" disabled value="<?php echo array_sum($arr);?>">

    <p>
        <input type="submit" class="btn btn-success" style="margin-top: 15px" value="Сохранить">
    </p>
    </form>
</div>

<div class="cash">
    <table border="1" class="table" id="work">
        <thead>
        <tr class="font">
            <td>Дата</td>
            <td>Администратор</td>
            <td>Сумма на начало</td>
            <td>Оборот</td>
            <td>Сумма на конец</td>
        </tr>
        </thead>
        <tbody>
            <?php
            foreach ($model as $value)
            {
                if(!isset($value))
                {
                   return false;
                }

                $minus = array_sum($arr)-$value['begin_saldo'];
                echo '<tr>';
                echo '<td>'.$value['payment_day'].'</td>';
                echo '<td>'.$value['administrator'].'</td>';
                echo '<td>'.$value['begin_saldo'].'</td>';
                echo '<td>'.$minus.'</td>';
                echo '<td>'.array_sum($arr).'</td>';
                echo '</tr>';
            }
            ?>

        </tbody>
    </table>

</div><!-- cash -->
<script type="text/javascript">
    $(document).ready(function () {
        $(".blocks").hide();
    });
    $('.btn-success').on('click', function () {
        $(".blocks").show();
    });
    var t = $('#example').DataTable();
    var counter = 1;
    $('#addRow').on( 'click', function () {
        t.row.add( [
            counter +'.1',
            counter +'.2',
            counter +'.3',
            counter +'.4',
            counter +'.5'
        ] ).draw( false );

        counter++;
    } );
</script>
</body>
</html>