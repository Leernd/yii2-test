<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\client_snp */

$this->title = 'Create Client Snp';
$this->params['breadcrumbs'][] = ['label' => 'Client Snps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-snp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
