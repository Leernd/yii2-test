<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cash */
/* @var $form ActiveForm */
?>
<div class="cash">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'payment_day') ?>
        <?= $form->field($model, 'updated_at') ?>
        <?= $form->field($model, 'created_at') ?>
        <?= $form->field($model, 'administrator_id') ?>
        <?= $form->field($model, 'begin_saldo') ?>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- cash -->
